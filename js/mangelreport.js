﻿function refill_lst_mangels(id) {
	if (id > 0) {
		set_new_class('loading_lst_equipments', '');
		var ajaxConn = new XHConn();
		ajaxConn.connect("ajaxeng.php?", "POST", "do=get_equipment_mangels&id="
				+ id, function(XML) {
			var data = XML.responseText;
			if (data.length > 0) {
				var arr = eval(data);
				var result = '';
				for ( var i = 0; i < arr.length; i++) {
					result += '<option value="' + arr[i].Title + '">'
							+ arr[i].Title + '</option>';
				}
				if (result.length <= 0) {
					mangel_changed('-1');
				}
				result += '<option value="-1">[Sonstig]</option>';
				document.getElementById("lst_mangels").innerHTML = result;
			}
			set_new_class('loading_lst_equipments', 'hidden');
		});
	} else {
		document.getElementById("lst_mangels").innerHTML = ''; 
	}
}

function mangel_changed(id) {
	if (id == -1) {
		set_new_class('new_mangel_tr', '');
		document.getElementById('new_mangel').focus();
		document.getElementById('new_mangel').select();
	} else {
		set_new_class('new_mangel_tr', 'hidden');
	}

}

function set_new_class(object_id, str_class) {
	if (navigator.appName == "Microsoft Internet Explorer") {
		document.getElementById(object_id).className = str_class;
	} else {
		document.getElementById(object_id).setAttribute("class", str_class);
	}
}