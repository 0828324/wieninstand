/** XHConn - Simple XMLHTTP Interface - bfults@gmail.com - 2005-04-08        **
 ** Code licensed under Creative Commons Attribution-ShareAlike License      **
 ** http://creativecommons.org/licenses/by-sa/2.0/                           **/
function XHConn() {
    var xmlhttp, bComplete = false;
    var rndXHConnNumber = Math.round(10000 * Math.random());
    try { xmlhttp = new ActiveXObject("Msxml2.XMLHTTP"); }
    catch (e) {
        try { xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); }
        catch (e) {
            try { xmlhttp = new XMLHttpRequest(); }
            catch (e) { xmlhttp = false; } 
        } 
    }
    if (!xmlhttp) return null;
    this.connect = function (sURL, sMethod, sVars, fnDone) {
        if (!xmlhttp) return false;
        bComplete = false;
        sMethod = sMethod.toUpperCase();
        try {
            if (sMethod == "GET") {
                xmlhttp.open(sMethod, sURL + "?" + sVars, true);
                sVars = "";

            } else if (sMethod == "FILE") {
                xmlhttp.open("POST", sURL, true);
                var form_data = new FormData();
                form_data.append('myFile', sVars);
                sVars = form_data; 

            } else {
                xmlhttp.open(sMethod, sURL, true);
                xmlhttp.setRequestHeader("Method", "POST " + sURL + "&rndXHConnNumber=" + rndXHConnNumber + " HTTP/1.1");
                //xmlhttp.setRequestHeader("Content-Type", "multipart/form-data");
                xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && !bComplete) {
                    bComplete = true;
                    fnDone(xmlhttp);
                }
            };
            xmlhttp.send(sVars);

        }
        catch (z) { return false; }
        return true;
    };
    return this;
}
