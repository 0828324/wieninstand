<?php
include 'db.php';
function getAnlagen($Anlagentyp='', $Ort='', $Strasse='', $PLZ='')  {
	$con=getConnect();
	$query = "SELECT * FROM Anlage WHERE Anlagentyp LIKE '%{$Anlagentyp}%' AND Ort LIKE '%{$Ort}%' AND Strasse LIKE '%{$Strasse}%' AND PLZ LIKE '%{$PLZ}%'";
	$result = mysqli_query($con, $query);
	$temp = '<tr><th>ID</th><th>Anlagentyp</th><th>Ort</th><th>Strasse</th><th>PLZ</th><th>Anlage Ausw&auml;hlen</th></tr>';
	while ($row = mysqli_fetch_array($result)) {
		$temp .= "<tr>";
		$temp .= "<td>".$row["ID"]."</td>";
		$temp .= "<td>".$row["Anlagentyp"]."</td>";
		$temp .= "<td>".$row["Ort"]."</td>";
		$temp .= "<td>".$row["Strasse"]."</td>";
		$temp .= "<td>".$row["PLZ"]."</td>";
		$temp .= '<td><form action="mangelreport.php" method="get">';
		$temp .= '<input name="ID" hidden="true" type="text" value="'.$row["ID"].'" size="10" maxlength="50">';
		$temp .= '<input class="btn" id="button" name="Mangel Einsenden" type="submit" value="Mangel Erstellen"></form></td>';
		$temp .= "</tr>";
	}
	$temp .= '</select>';
	echo $temp;
	mysqli_close($con);
}

function getSingleAnlage($ID) {
	$con=getConnect();
	$query = "SELECT * FROM Anlage WHERE ID = $ID";
	$result = mysqli_query($con, $query);
	return mysqli_fetch_array($result);
}

function get_anlage_equipments_list($ID) {
	$con=getConnect();
	$query = "SELECT * FROM Equipments WHERE Anlage_ID = $ID";
	$result = mysqli_query($con, $query);
	$temp ="";
	while ($row = mysqli_fetch_array($result)) {
		$temp .= "<option value=\"".$row["ID"]."\">".$row["Title"]."</option>";
	}
	mysqli_close($con);
	return $temp;
}

function db_get_equipment_mangels_list($ID) {
	$con=getConnect();
	$query = "SELECT * FROM Mangel WHERE Equipment_ID = $ID  GROUP BY(Title)";
	$result = mysqli_query($con, $query);
	$data = array();
	while ($row = mysqli_fetch_array($result)) {
		$data[] = $row;
	}
	mysqli_close($con);
	return $data;
}
function db_insert_mangel_report($equipment_id,$title,$description,$photo,$user_id,$status) {
	$con=getConnect();
	$query = "INSERT INTO Mangel(Equipment_ID, Title, Description, Photo, User_ID, Status, ReportDate)VALUES($equipment_id,'$title','$description','$photo',$user_id,'$status','".date('Y/m/d H:i:s')."')";
	$result = mysqli_query($con, $query);
	if (!$result)
	{
		die('Error: [db_insert_mangel_report]' . mysqli_error($con));
	}
	return mysqli_insert_id($con);
	mysqli_close($con);
}
function db_insert_FeedbackSubscription($user_id,$mangel_id) {
	$con=getConnect();
	$query = "INSERT INTO FeedbackSubscription(User_ID, Mangel_ID)VALUES($user_id,$mangel_id)";
	$result = mysqli_query($con, $query);
	if (!$result)
	{
		die('Error: [db_insert_FeedbackSubscription] ' . mysqli_error($con));
	}
	return 1;
	mysqli_close($con);
}
?>