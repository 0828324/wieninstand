﻿<?php
    $mangel ='';
	$anlage ='';
	$user ='';
	$status ='';
	if(isset($_GET['refresh']))
	{
	    $mangel =$_GET['mangel'];
		$anlage =$_GET['anlage'];
		$user =$_GET['user'];
		$status =$_GET['status'];
	}   
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Mangelsuche</title>
<link rel="shortcut icon" type="image/x-icon" href="../images/favicon.ico">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<link rel="stylesheet" type="text/css" href="css/table.css">
<script src="js/jquery-latest.js"></script>
<script src="js/jquery.tablesorter.js"></script>
<script src="js/script.js"></script>
</head>
<body id="mangelsuche">
<div id="wrapper">
<?php 
include 'menu.php'; 
include 'dbconnection.php'; 
?>
<div id="textbereich">
<h1>Mangelsuche</h1>
<!--<h3>Filter</h3>-->

<form action="Mangelsuche.php" method="get">
<table border="0">
	<tr>
	    <th>Mangel:</th>
		<th>Anlagentyp:</th>
		<th>User:</th>
		<th>Status:</th>
	</tr>
	<tr>
	    <td><input class="txt" name="mangel" type="text" value="<?php echo $mangel;?>" size="25" maxlength="50"></td>
		<td><input class="txt" name="anlage" type="text" value="<?php echo $anlage;?>" size="25" maxlength="50"></td>
		<td><input class="txt" name="user" type="text" value="<?php echo $user;?>" size="25" maxlength="50"></td>
		<td><input class="txt" name="status" type="text" value="<?php echo $status;?>" size="25" maxlength="50"></td>
	</tr>
	<tr>
		<td><input class="btn" id="button" name="refresh" type="submit" value="Filter Anwenden"></td>
	</tr>
</table>
</form>
<h3>Mangel</h3>
<table id="table_mangel" class="tablesorter">
	<?php 

	
	if(isset($_GET['refresh']))
	{
		getMangel($mangel,$anlage,$user,$status);
	} 
	else 
	{
		getMangel();
	}                           
	?>
</table>
</div>
</div>
</body>
</html>
