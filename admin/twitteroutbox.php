<!DOCTYPE html>
<?php
include_once 'twitter/twitterUtil.php';

$twitterUtil = new twitterUtil();
?>
<html>
<head>
<title>Twitter</title>
<link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body id="twitteroutbox">
<div id="wrapper">
<?php include 'menu.php'; ?>
<?php include 'twitter/subMenu.php'; ?>
    <div id="textbereich">
        <?php
            
            if (isset($_POST['text'])) {
                $twitterUtil->sendTweet($_POST['text']);
            }
        ?>
        <table class="anlagensuche">
            <tr>
                <th>Datum</th>
                <th>An</th>
                <th>Text</th>
                
            </tr>
            <?php
                $rows = getSentTweets();
                foreach ($rows as $tweet) {
                    echo '<tr><td width="120px">'.$tweet->created_at
                            .'</td><td width="120px">'
                            .$tweet->to_user
                            .'</td><td>'
                            .$tweet->text
                            .'</td></tr>';
                }
            ?>
        </table>
    </div>
</body>
</html>
