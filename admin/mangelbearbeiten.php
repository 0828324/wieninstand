<?php
include 'dbconnection.php';
include_once 'twitter/twitterUtil.php';

$sendTweet = false;
if($_GET["action"]=="Status Updaten"){
	$retStatus;
	if($_GET["comment"]!="") {
		$retStatus = $_GET["statusUpdate"]." (".$_GET["comment"].")";
	} else {
		$retStatus = $_GET["statusUpdate"];
	}
	//$_GET["statusUpdate"]
	changeMangelStatus($_GET["id"],$retStatus);
    if (strcmp($retStatus,"Erledigt") == 0 ) {
        $sendTweet = true;
    }
}

$mangel = getSingleMangel($_GET["id"]);

if ($sendTweet) {
    $message = "#".$mangel["anlagentyp"]."_".$mangel["plz"]."_".$mangel["strasse"].": "
        ."Mangel \"".$mangel["mangel"]."\" wurde behoben.";
    if(isset($_GET["comment"]) && strlen($_GET["comment"]) > 0) {
        $message .= "Kommentar: ".$_GET["comment"];
    }
        
    $twitterUtil = new twitterUtil();
    $twitterUtil->sendTweet(
        $message,
        null, null);
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Mangel bearbeiten</title>
<link rel="shortcut icon" type="image/x-icon" href="../images/favicon.ico">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body id="mangelbearbeiten">
	<div id="wrapper">
	<?php include 'menu.php'; ?>
		<div id="textbereich">

			<h1>Mangel&uuml;bersicht</h1>
			<div
				style="float: left; padding: 5px; border: 1px solid gray; margin: 0 10px 0 0;">
				<a title="Click to view in large size" href="../<?php echo str_replace("-thumb","-orginal",$mangel["photo"])?>">
				<img src="../<?php echo $mangel["photo"]?>" />
				</a>
			</div>
			<table>
				<!--
	<tr>
		<td>ID:</td>
		<td><?php echo $mangel["ID"]?></td>
	</tr>
//-->
				<tr>
					<th>Mangel:</th>
					<td><?php echo $mangel["mangel"]?></td>
				</tr>

				<tr>
					<th>Beschreibung:</th>
					<td><?php echo $mangel["description"]?></td>
				</tr>

				<tr>
					<th>Equipment:</th>
					<td><?php echo $mangel["equipment"]?></td>
				</tr>

				<tr>
					<th>Anlagentyp:</th>
					<td><?php echo $mangel["anlagentyp"]?></td>
				</tr>

				<tr>
					<th>Strasse:</th>
					<td><?php echo $mangel["strasse"]?></td>
				</tr>

				<tr>
					<th>Photo:</th>
					<td><a href="../<?php echo str_replace("-thumb","-orginal",$mangel["photo"])?>"><?php echo $mangel["photo"]?>
					</a></td>
				</tr>

				<tr>
					<th>User:</th>
					<td><?php echo $mangel["user"]?></td>
				</tr>

				<tr>
					<th>Report Date:</th>
					<td><?php echo $mangel["reportdate"]?></td>
				</tr>

				<tr>
					<th>Status:</th>
					<td><?php echo $mangel["status"]?></td>
				</tr>
				<!--
	<tr>

	    <td>
		    <form action="Mangelbearbeiten.php" method="get">
			    <input type="hidden" name="id" id="id" type="text" value="<?php echo $mangel["id"]?>">
		        <input class="btn" id="button" name="action" type="submit" value="Status &Auml;ndern">
			</form>
		</td>
		
		<td>
			<form action="Mangelbearbeiten.php" method="get">
			    <input type="hidden" name="id" id="id" type="text" value="<?php echo $mangel["id"]?>">
		        <input class="btn" id="button" name="action" type="submit" value="re-open">
			</form>
		</td>
		
	</tr>
	-->

			</table>

			<h1>Mangel bearbeiten</h1>
			<form action="mangelbearbeiten.php" method="get">
				<table>
					<tr>
						<th>Status Ausw&auml;hlen:</th>
						<td><select name="statusUpdate" size="1">
								<option>Offen</option>
								<option>In Bearbeitung</option>
								<option>Zur&uuml;ckgewiesen</option>
								<option>Erledigt</option>
						</select>
						</td>
					</tr>
					<tr>
						<th>Kommentar (optional):</th>
						<td><input name="comment" type="text" value="" size="25"
							maxlength="50"></td>
					</tr>
					<tr>
						<td><input type="hidden" name="id" id="id"
							value="<?php echo $mangel["id"]?>"> <input class="btn"
							id="button" name="action" type="submit" value="Status Updaten">
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</body>
</html>
