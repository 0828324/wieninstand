<?php
include_once 'twitter/tweet.php';
include '../db.php';
error_reporting(E_WARNING);

function getAnlagen($Anlagentyp='', $Ort='', $Strasse='', $PLZ='')  {
	$con=getConnect();
	$query = "SELECT * FROM Anlage WHERE Anlagentyp LIKE '%{$Anlagentyp}%' AND Ort LIKE '%{$Ort}%' AND Strasse LIKE '%{$Strasse}%' AND PLZ LIKE '%{$PLZ}%'";
	$result = mysqli_query($con, $query);
	$temp = '<thead><tr><th>ID</th><th>Anlagentyp</th><th>Ort</th><th>Strasse</th><th>PLZ</th><th>Anlage Ausw&auml;hlen</th></tr></thead>';
	while ($row = mysqli_fetch_array($result)) {
		$temp .= "<tr>";
		$temp .= "<td>".$row["ID"]."</td>";
		$temp .= "<td>".$row["Anlagentyp"]."</td>";
		$temp .= "<td>".$row["Ort"]."</td>";
		$temp .= "<td>".$row["Strasse"]."</td>";
		$temp .= "<td>".$row["PLZ"]."</td>";
		$temp .= '<td><form action="mangelreport.php" method="get">';
		$temp .= '<input name="ID" type="hidden" value="'.$row["ID"].'" size="10" maxlength="50">';
		$temp .= '<input class="btn" id="button" name="Mangel Einsenden" type="submit" value="Mangel Erstellen"></form></td>';
		$temp .= "</tr>";
	}
	$temp .= '</select>';
	echo $temp;
	mysqli_close($con);
}

function getSingleAnlage($ID) {
	$con=getConnect();
	$query = "SELECT * FROM Anlage WHERE ID = $ID";
	$result = mysqli_query($con, $query);
	return mysqli_fetch_array($result);
}

function getMangel($mangel='', $anlage='', $user='', $status='')  {
	$con=getConnect();
	$query = "select mm.prioritaet,mm.id,aa.anlagentyp as 'anlagentyp',aa.strasse as 'strasse',ee.title as 'equipment',mm.title as 'mangel',mm.description,mm.photo,uu.name as 'user',mm.status,mm.reportdate from Mangel as mm left join Equipments as ee on mm.equipment_id=ee.id left join Anlage as aa on ee.anlage_id=aa.id left join Users as uu on mm.user_id=uu.id where mm.title like '%{$mangel}%' and aa.anlagentyp like '%{$anlage}%' and uu.name like '%{$user}%' and mm.status like '%{$status}%' ORDER BY mm.prioritaet DESC;";
	$result = mysqli_query($con, $query);
	$temp = '<thead><tr><th>Prioritaet </th><th>Mangel</th><th>Description</th><th>Equipment</th><th>Anlagentyp</th><th>Strasse</th><th>Photo</th><th>User</th><th>status</th><th>ReportDate</th><th>Mangel Ausw&auml;hlen</th></tr></thead>';
	while ($row = mysqli_fetch_array($result)) {
		$temp .= "<tr>";
		$temp .= "<td>".$row["prioritaet"]."</td>";
		$temp .= "<td>".$row["mangel"]."</td>";
		$temp .= "<td>".$row["description"]."</td>";
		$temp .= "<td>".$row["equipment"]."</td>";
		$temp .= "<td>".$row["anlagentyp"]."</td>";
		$temp .= "<td>".$row["strasse"]."</td>";
		if(isset($row["photo"]))
		$temp .= "<td><a class=\"photo\" href='../".$row["photo"]."'>Photo</a></td>";
		else
		$temp .= "<td>&nbsp;</td>";

		$temp .= "<td>".$row["user"]."</td>";
		$temp .= "<td>".$row["status"]."</td>";
		$temp .= "<td>".$row["reportdate"]."</td>";
		$temp .= '<td><form action="mangelbearbeiten.php" method="get">';
		$temp .= '<input name="id" hidden="true" type="text" value="'.$row["id"].'" size="10" maxlength="50">';
		$temp .= '<input class="btn" id="button" name="action" type="submit" value="Mangel Bearbeiten"></form></td>';
		$temp .= "</tr>";
	}

	echo $temp;
	mysqli_close($con);
}

function getSingleMangel($id) {
	$con=getConnect();
	$query = "select mm.id,aa.anlagentyp as 'anlagentyp',aa.strasse as 'strasse',aa.plz as 'plz',ee.title as 'equipment',mm.title as 'mangel',mm.description,mm.photo,uu.name as 'user',mm.status,mm.reportdate from Mangel as mm left join Equipments as ee on mm.equipment_id=ee.id left join Anlage as aa on ee.anlage_id=aa.id left join Users as uu on mm.user_id=uu.id where mm.id=$id;";
	$result = mysqli_query($con, $query);
	return mysqli_fetch_array($result);
}

function changeMangelStatus($id, $status) {
	$con=getConnect();
	$query = "update mangel set status='$status' where id=$id;";
	$result = mysqli_query($con, $query);
}

function saveTweet($tweet) {
     $db = getPDOConnect();
     $stmt = $db->prepare("INSERT INTO tweet (id, created_at, from_user, from_user_id,
         to_user, to_user_id, text, in_reply_to_status_id) VALUES (?,?,?,?,?,?,?,?)");
     $result = $stmt->execute($tweet->getArray());
     if ($result === false) {
         error_log("Failed to saveTweet");
     }
 }
 
function getSentTweets() {
    $db = getPDOConnect();
    $stmt = $db->query('SELECT * FROM tweet WHERE from_user = "wieninstand" ORDER BY created_at DESC');
    $retval = array();
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $tweet = new tweet(
            $row['ID'],
            $row['CREATED_AT'],
            $row['FROM_USER'],
            $row['FROM_USER_ID'],
            $row['TO_USER'],
            $row['TO_USER_ID'],
            $row['TEXT'],
            $row['IN_REPLY_TO_STATUS_ID']             
        );
        $retval[] = $tweet;
    }
    return $retval;
}

function getReceivedTweets() {
    $db = getPDOConnect();
    $stmt = $db->query('SELECT * FROM tweet WHERE to_user = "wieninstand" ORDER BY created_at DESC');
    $retval = array();
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $tweet = new tweet(
            $row['ID'],
            $row['CREATED_AT'],
            $row['FROM_USER'],
            $row['FROM_USER_ID'],
            $row['TO_USER'],
            $row['TO_USER_ID'],
            $row['TEXT'],
            $row['IN_REPLY_TO_STATUS_ID']             
        );
        $retval[] = $tweet;
    }
    return $retval;
}

function getLastIncomingTweetId() {
    $db = getPDOConnect();
    $stmt = $db->query('SELECT max(id) FROM tweet WHERE from_user <> "wieninstand"');
    $stmt->execute();
    
    if ($ret = $stmt->fetchColumn()) {
        return $ret;
    }
    return 0;    
}

?>
