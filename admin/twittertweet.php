<!DOCTYPE html>
<?php
include_once 'twitter/twitterUtil.php';

$twitterUtil = new twitterUtil();
?>
<html>
<head>
<title>Twitter</title>
<link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body id="twittertweet">
<div id="wrapper">
<?php include 'menu.php'; ?>
<?php include 'twitter/subMenu.php'; ?>
    <div id="textbereich">
        <h1>Tweet senden</h1>
        <?php
        if (isset($_POST['text'])) {
            $result = $twitterUtil->sendTweet($_POST['text'], $_POST['to_user'], $_POST['to_user_id']);
           if ($result == 0) {
            
                echo 
                '<p>
                    Der Tweet wurde erfolgreich gesendet.<br />
                    Sie k&ouml;nnen den Tweet unter <a href="https://twitter.com/wieninstand" target="_blank">https://twitter.com/wieninstand</a> einsehen.
                </p>';
                
            } else {
                echo 'Der Tweet konnte nicht &uuml;bertragen werden. Bitte versuchen Sie es erneut oder kontaktieren Sie einen Administrator.';
            }
        }
        ?>

        <form action="twittertweet.php" method="post" accept-charset="utf-8">
            <p>
                <label for="text">Neuer Tweet:</label>
            </p>
            <p>
                <input type="hidden" name="to_user" value="<?php if (isset($_GET['to_user']) && strlen($_GET['to_user']) <= 15) { echo $_GET['to_user']; } ?>">
                <input type="hidden" name="to_user_id" value="<?php if (isset($_GET['to_user_id'])) { echo $_GET['to_user_id']; } ?>">
                <textarea name="text" type="text" rows="6" cols="50" maxlength="140" ><?php if (isset($_GET['to_user']) && strlen($_GET['to_user']) <= 15) {echo '@'.htmlentities($_GET['to_user']);} ?></textarea>
            </p>
            <p>
                <input class="btn" type="submit" value="Absenden" />
            </p>
        </form>
    </div>
</body>
</html>