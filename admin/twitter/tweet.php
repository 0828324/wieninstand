<?php 
class tweet {
    public $id;
    public $created_at;
    public $from_user;
    public $from_user_id;
    public $to_user;
    public $to_user_id;
    public $text;
    public $in_reply_to_status_id;
    
    public function __construct($id, $created_at, $from_user, $from_user_id,
            $to_user, $to_user_id, $text, $in_reply_to_status_id) {  
        $this->id = $id;
        $this->created_at = $created_at;
        $this->from_user = $from_user;
        $this->from_user_id = $from_user_id;
        $this->to_user = $to_user;
        $this->to_user_id = $to_user_id;
        $this->text = $text;
        $this->in_reply_to_status_id = $in_reply_to_status_id;
    }
    
    public function getArray() {
        return array($this->id, $this->created_at, $this->from_user, $this->from_user_id, 
            $this->to_user, $this->to_user_id, $this->text, $this->in_reply_to_status_id, );
    }
}
?>
