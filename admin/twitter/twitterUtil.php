<?php 
include_once 'tmhOAuth.php';
include_once 'tmhUtilities.php';
include_once 'dbconnection.php';
include_once 'tweet.php';

class twitterUtil {
    
    private $auth;
    private $username;
    private $user_id;

    public function __construct() {  
        $this->auth = new tmhOAuth(array(
            'consumer_key' => 'Y41HCBsHnz5yM7FKf9pgg',
            'consumer_secret' => 'fICDLUQL2PNo41y2K6DSGN25d75O1EuCnWGYQ6u8d2U',
            'user_token' => '1490728345-G5H2Vk3RScfg7BIe9pCSKeH4hfatHEpLuG8jUJc',
            'user_secret' => 'VJ9yeTomzFCxRFbb4IuH8jej2aD3RNrE4w4QkFyM8o'
        ));
        $this->username = 'wieninstand';
        $this->user_id = (int)1490728345;
    }
    
    public function sendTweet($message, $to_user, $to_user_id) {
        if (strlen($message) > 140) {
            return 1;
        }
        $response = $this->auth->request('POST', $this->auth->url('1.1/statuses/update'), array(
            'status' => $message
        ));
        if ($response != 200) {
            error_log("Failed to send tweet ", 0);
            return 1;
        } else {
            $r = json_decode($this->auth->response['response']);
            $created_at = new DateTime($r->created_at);
            $created_at->setTimezone(new DateTimeZone('Europe/Vienna'));
            if ($to_user === '') {
                $to_user = null;
            }
            if ($to_user_id === '') {
                $to_user_id = null;
            }
            
            $tweet = new tweet(
                $r->id_str,
                $created_at->format('Y-m-d H:i:s'),
                $this->username,
                $this->user_id,
                htmlentities($to_user),
                htmlentities($to_user_id),
                htmlentities($message),
                null
            );
            saveTweet($tweet);
            return 0;
        }
    }
    
    public function getFollowerCount() {
        $response = $this->auth->request('GET', $this->auth->url('1.1/users/show.json'), array(
            'screen_name' => $this->username
        ));
        if ($response != 200) {
            error_log("Failed to send tweet ", 0);
            return -1;
        } else {
            return json_decode($this->auth->response['response'])->followers_count;
        }
    }
    
    public function getNewIncomingTweets() {
        $since_id = getLastIncomingTweetId();
        $params = array(
            'q' => '@wieninstand',
            'since_id' => $since_id,
            'count' => '1000',
        );

        $response = $this->auth->request('GET', $this->auth->url('1.1/search/tweets.json'), $params);
        
        if ($response != 200) {
            error_log("Failed to retrieve tweets ", 0);
        } else {
            $result = json_decode($this->auth->response['response'], true);
            foreach($result['statuses'] as $row) {
                
                $created_at = new DateTime($row['created_at']);
                $created_at->setTimezone(new DateTimeZone('Europe/Vienna'));
                
                $tweet = new tweet(
                    $row['id_str'], 
                    $created_at->format('Y-m-d H:i:s'),
                    $row['user']['screen_name'],
                    $row['user']['id_str'],
                    $this->username,
                    $this->user_id['to_user_id'],
                    htmlentities($row['text']),
                    null
                );
                saveTweet($tweet);    
            }
        }
    }
    
}

?>
