<!DOCTYPE html>
<?php
include_once 'twitter/twitterUtil.php';

$twitterUtil = new twitterUtil();
?>
<html>
<head>
<title>Twitter</title>
<link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body id="twitterinbox">
<div id="wrapper">
<?php include 'menu.php'; ?>
<?php include 'twitter/subMenu.php'; ?>
    <div id="textbereich">
        <?php
            if (isset($_POST['retrieve'])) {
                $twitterUtil->getNewIncomingTweets();
            }
        ?>
        <table class="anlagensuche">
            <tr>
                <th>Datum</th>
                <th>Von</th>
                <th>Text</th>
                <th></th>
            </tr>
            <?php
                $rows = getReceivedTweets();
                foreach ($rows as $tweet) {
                echo '<tr><td width="120px">'.$tweet->created_at.'</td><td width="120px">'
                        .$tweet->from_user.'</td><td>'.$tweet->text.'</td><td width="80px">'
                        .'<form method="get" action="twittertweet.php">'
                        .'<input type="hidden" name="to_user" value='.$tweet->from_user
                        .' /><input type="hidden" name="to_user_id" value='
                        .$tweet->from_user_id.' /><input id="button" class="btn" type="submit" value="Antworten" name="action"></form></td></tr>';
                }
            ?>
        </table>
        <form action="twitterinbox.php" method="post">
            <p>
                <input type="hidden" name="retrieve" value=""/>
                <input class="btn" type="submit" value="Tweets abrufen" />
            </p>
        </form>
    </div>
</body>
</html>
