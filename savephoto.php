<?php
function save_photo(){
	if (($_FILES["file_photo"]["type"] == "image/jpeg" || $_FILES["file_photo"]["type"] == "image/pjpeg" || $_FILES["file_photo"]["type"] == "image/gif" || $_FILES["file_photo"]["type"] == "image/x-png") && ($_FILES["file_photo"]["size"] < 4000000))
	$current_img=$_FILES['file_photo']['name'];
	$extension = substr(strrchr($current_img, '.'), 1);
	date_default_timezone_set("Europe/Stockholm");
	$time = date("fYhis"); 
	$new_image = uniqid() . $time;

	$originalImage  = "photos/".$new_image . "-orginal" . "." . $extension;
	$destination   = "photos/".$new_image . "-thumb" . "." . $extension;

	$action = move_uploaded_file($_FILES['file_photo']['tmp_name'], $originalImage);

	$max_upload_width = 120;
	$max_upload_height = 120;
	if($_FILES["file_photo"]["type"] == "image/jpeg" || $_FILES["image"]["type"] == "image/pjpeg"){
		$image_source = imagecreatefromjpeg($originalImage) ;
	}
	if($_FILES["file_photo"]["type"] == "image/gif"){
		$image_source = imagecreatefromgif($originalImage);
	}
	if($_FILES["file_photo"]["type"] == "image/bmp"){
		$image_source = imagecreatefromwbmp($originalImage);
	}
	if($_FILES["file_photo"]["type"] == "image/x-png"){
		$image_source = imagecreatefrompng($originalImage);
	}

	imagejpeg($image_source,$destination,100);
	chmod($destination,0644);

	list($image_width, $image_height) = getimagesize($destination);

	if($image_width>$max_upload_width || $image_height >$max_upload_height){
		$proportions = 1;

		if($image_width>$image_height){
			$new_width  = $max_upload_width;
			$new_height = round($max_upload_width/$proportions);
		}
		else{
			$new_height = $max_upload_height;
			$new_width  = round($max_upload_height*$proportions);
		}


		$new_image = imagecreatetruecolor($new_width , $new_height);
		$image_source = imagecreatefromjpeg($destination);

		imagecopyresampled($new_image, $image_source, 0, 0, 0, 0, $new_width, $new_height, $image_width, $image_height);
		imagejpeg($new_image, $destination, 100); // save
		imagedestroy($new_image);
		return $destination;
	}
}
?>