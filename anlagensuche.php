﻿<?php
	$Anlagentyp ='';
	$Ort ='';
	$Strasse ='';
	$PLZ ='';
	if(isset($_GET['refresh']))
	{
		$Anlagentyp =$_GET['Anlagentyp'];
		$Ort =$_GET['Ort'];
		$Strasse =$_GET['Strasse'];
		$PLZ =$_GET['PLZ'];
	}   
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Anlagensuche</title>
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head> 
<body id="anlagensuche">
<div id="wrapper">
<?php include 'menu.php'; ?>
<div id="textbereich">
<h1>Anlagensuche</h1>
<!--<h3>Filter</h3>-->

<form action="anlagensuche.php" method="get">
<table border="0">
	<tr>
		<th>Anlagentyp:</th>
		<th>Ort:</th>
		<th>Stra&szlig;e:</th>
		<th>PLZ:</th>
	</tr>
	<tr>
		<td><input class="txt" name="Anlagentyp" type="text" value="<?php echo $Anlagentyp;?>" size="25" maxlength="50"></td>
		<td><input class="txt" name="Ort" type="text" value="<?php echo $Ort;?>" size="25" maxlength="50"></td>
		<td><input class="txt" name="Strasse" type="text" value="<?php echo $Strasse;?>" size="25" maxlength="50"></td>
		<td><input class="txt" name="PLZ" type="text" value="<?php echo $PLZ;?>" size="10" maxlength="50"></td>
	</tr>
	<tr>
		<td><input class="btn" id="button" name="refresh" type="submit" value="Filter Anwenden"></td>
	</tr>
</table>
</form>
<h3>Anlagen</h3>
<table class="anlagensuche">
	<?php 

	include 'dbconnection.php'; 
	if(isset($_GET['refresh']))
	{
		getAnlagen($_GET["Anlagentyp"],$_GET["Ort"], $_GET["Strasse"], $_GET["PLZ"]);
	} 
	else 
	{
		getAnlagen();
	}                           
	?>
</table>
</div>
</div>
</body>
</html>
