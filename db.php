<?php

define("DB_HOST","localhost");
define("DB_NAME","wieninstanddb");
define("DB_USER","root");
define("DB_PASS","");

function getConnect(){
	$con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if (mysqli_connect_errno())
    {
  	    echo "Failed to connect to MySQL: " . mysqli_connect_error();
		return null;
    }	
	
	return $con;
}

function getPDOConnect() {
    return new PDO('mysql:host='.DB_HOST.'; dbname='.DB_NAME, DB_USER, DB_PASS);
}
?>