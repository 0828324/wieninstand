/* Anlage */
INSERT INTO Anlage (Anlagentyp, Ort, Strasse, PLZ) VALUES('Beachvolleyballplatz', 'Favoriten, Waldm&uuml;llerpark' ,'Dampfgasse', '1100' );
INSERT INTO Anlage (Anlagentyp, Ort, Strasse, PLZ) VALUES('Tischtennisplatte', 'Favoriten, Waldm&uuml;llerpark' ,'Dampfgasse', '1100' );
INSERT INTO Anlage (Anlagentyp, Ort, Strasse, PLZ) VALUES('Basketballplatz', 'Favoriten, Waldm&uuml;llerpark' ,'Dampfgasse', '1100' );
INSERT INTO Anlage (Anlagentyp, Ort, Strasse, PLZ) VALUES('Fu&szlig;ballk&auml;fig', 'Brigittenau, Donauinsel' ,'unbekannt', '1200' );
INSERT INTO Anlage (Anlagentyp, Ort, Strasse, PLZ) VALUES('Beachvolleyballplatz', 'Brigittenau, Donauinsel' ,'unbekannt', '1200' );


/* Equipments */
INSERT INTO Equipments (Anlage_ID, Title, Description) VALUES(1, 'Netz' ,'Netzmontage zwischen den Stahlpfosten');
INSERT INTO Equipments (Anlage_ID, Title, Description) VALUES(1, 'Linien' ,'Linien zur begrenzung des Spielfeldes');
INSERT INTO Equipments (Anlage_ID, Title, Description) VALUES(1, 'Sand' ,'Probleme mit den beschaffenheiten des Sandes (zB Verschmutzung)');
INSERT INTO Equipments (Anlage_ID, Title, Description) VALUES(2, 'Metallnetz' ,'Netzmontage um die Spielfl&auml;che abzuteilen');
INSERT INTO Equipments (Anlage_ID, Title, Description) VALUES(3, 'K&ouml;rbe' ,'Korbmontage aus Metall');
INSERT INTO Equipments (Anlage_ID, Title, Description) VALUES(4, 'Tor' ,'Torgestell aus Metall');
INSERT INTO Equipments (Anlage_ID, Title, Description) VALUES(5, 'Netz' ,'Netzmontage zwischen den Stahlpfosten');


/* Users */
INSERT INTO Users (Name, Email, Pass, RegDate) VALUES('Alex', 'alex@gmail.com', '123' ,'2013-01-01 04:30:00');
INSERT INTO Users (Name, Email, Pass, RegDate) VALUES('Chris', 'chris@gmail.com', '123' ,'2013-02-02 01:10:00');
INSERT INTO Users (Name, Email, Pass, RegDate) VALUES('Max', 'max@gmail.com', '123' ,'2013-01-01 02:10:10');

/* Mangel */
INSERT INTO Mangel (Equipment_ID, Title, Description, Photo, User_ID, ReportDate) VALUES(1, 'Spannseil gerissen' ,'Die Spannvorrichtung f&uuml;r das Netz ist abgerissen', 'photos/0x1.jpg',1,'2013-02-06 12:25:00');
INSERT INTO Mangel (Equipment_ID, Title, Description, Photo, User_ID, ReportDate) VALUES(2, 'Linien fehlen' ,'Es waren keine Linien Vorort', 'photos/0x2.jpg',1,'2013-03-06 18:45:00');
INSERT INTO Mangel (Equipment_ID, Title, Description, Photo, User_ID, ReportDate) VALUES(3, 'Glascherben' ,'Es befinden sich haufenweise Glasscherben im Sand.', 'photos/0x3.jpg',2,'2013-02-05 12:01:00');
INSERT INTO Mangel (Equipment_ID, Title, Description, Photo, User_ID, ReportDate) VALUES(3, 'Pf&uuml;tze' ,'Es steht eine gro&ouml;e Wasserpf&auml;tze direkt am Platz.', 'photos/0x4.jpg',2,'2013-05-13 19:30:00');

/* FeedbackSubscription */

/* Tweets */
INSERT INTO Tweet (ID, CREATED_AT, FROM_USER, FROM_USER_ID, TO_USER, TO_USER_ID, TEXT, IN_REPLY_TO_STATUS_ID) VALUES ('343282724888530944', '2013-06-08 10:25:54', 'WAEtester', 1492296228, 'wieninstand', 1490728345, '@wieninstand Danke sehr !', NULL);
INSERT INTO Tweet (ID, CREATED_AT, FROM_USER, FROM_USER_ID, TO_USER, TO_USER_ID, TEXT, IN_REPLY_TO_STATUS_ID) VALUES ('343717904648396800', '2013-06-09 15:15:09', 'wieninstand', 1490728345, NULL, NULL, '#Beachvolleyballplatz_1100_Dampfgasse wurde der Sand erneuert.', NULL);
INSERT INTO Tweet (ID, CREATED_AT, FROM_USER, FROM_USER_ID, TO_USER, TO_USER_ID, TEXT, IN_REPLY_TO_STATUS_ID) VALUES ('344046205044682752', '2013-06-10 12:59:42', 'wieninstand', 1490728345, NULL, NULL, 'Am #Beachvolleyballplatz_1200_Donauinsel wurde ein neues Netz angebracht.', NULL);
INSERT INTO Tweet (ID, CREATED_AT, FROM_USER, FROM_USER_ID, TO_USER, TO_USER_ID, TEXT, IN_REPLY_TO_STATUS_ID) VALUES ('344047319181819905', '2013-06-10 13:04:07', 'wieninstand', 1490728345, NULL, NULL, 'Die #Tischtennisplatte_1100_Dampfgasse wurde gereinigt, repariert und neu lackiert. Viel Spass bei Spielen !', NULL);
INSERT INTO Tweet (ID, CREATED_AT, FROM_USER, FROM_USER_ID, TO_USER, TO_USER_ID, TEXT, IN_REPLY_TO_STATUS_ID) VALUES ('344360879304884224', '2013-06-11 09:50:06', 'wieninstand', 1490728345, NULL, NULL, 'Eine #Tischtennisplatte_1100_Dampfgasse wird Anfang Juli ersetzt.', NULL);
INSERT INTO Tweet (ID, CREATED_AT, FROM_USER, FROM_USER_ID, TO_USER, TO_USER_ID, TEXT, IN_REPLY_TO_STATUS_ID) VALUES ('344361115792322561', '2013-06-11 09:51:02', 'WAEtester', 1492296228, 'wieninstand', 1490728345, '@wieninstand Neue Platte ? Das ist grossartig !', NULL);
INSERT INTO Tweet (ID, CREATED_AT, FROM_USER, FROM_USER_ID, TO_USER, TO_USER_ID, TEXT, IN_REPLY_TO_STATUS_ID) VALUES ('344361473209925632', '2013-06-11 09:52:27', 'wieninstand', 1490728345, 'WAEtester', 1492296228, '@WAEtester Die neuen Netze kommen aber leider erst Mitte Juli.', NULL);
INSERT INTO Tweet (ID, CREATED_AT, FROM_USER, FROM_USER_ID, TO_USER, TO_USER_ID, TEXT, IN_REPLY_TO_STATUS_ID) VALUES ('344364542190563328', '2013-06-11 10:04:39', 'wieninstand', 1490728345, 'WAEtester', 1492296228, '@WAEtester Danke f&uuml;r das Feedback!', NULL);