CREATE TABLE Anlage (
        ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        Anlagentyp VARCHAR(100),
        Ort VARCHAR(100),
        Strasse VARCHAR(100),
        PLZ VARCHAR(8),
        Photo VARCHAR(100)
        );
CREATE TABLE Equipments (
        ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        Anlage_ID INT,
        Title VARCHAR(100),
        Description VARCHAR(300)
        );
CREATE TABLE Users (
        ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        Name VARCHAR(100),
        Email VARCHAR(200),
        Pass VARCHAR(100),
        RegDate DATETIME
        );      
        
CREATE TABLE Mangel (
        ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        Equipment_ID INT,
        Title VARCHAR(100),
        Description VARCHAR(300),
        Photo VARCHAR(100),     
        User_ID INT,
		Status VARCHAR(50) NOT NULL DEFAULT 'Offen',
        ReportDate DATETIME,
        Prioritaet INT NOT NULL DEFAULT 0,
        FOREIGN KEY (User_ID) REFERENCES Users(ID),
        FOREIGN KEY (Equipment_ID) REFERENCES Equipments(ID)
        );

        
CREATE TABLE FeedbackSubscription (
        User_ID INT,
        Mangel_ID INT,
        Feedback boolean,
        FOREIGN KEY (User_ID) REFERENCES Users(ID),
        FOREIGN KEY (Mangel_ID) REFERENCES Mangel(ID)
        );
        
CREATE TABLE IF NOT EXISTS Tweet (
        ID varchar(30) NOT NULL,
        CREATED_AT datetime NOT NULL,
        FROM_USER varchar(15) NOT NULL,
        FROM_USER_ID int(11) NOT NULL,
        TO_USER varchar(15) DEFAULT NULL,
        TO_USER_ID int(11) DEFAULT NULL,
        TEXT varchar(140) NOT NULL,
        IN_REPLY_TO_STATUS_ID int(11) DEFAULT NULL,
        PRIMARY KEY (ID)
        );