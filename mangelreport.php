<?php
session_start();
include 'dbconnection.php';
include 'savephoto.php';
function getMangel($anlage)  {
	$mangel='';
	$user='';
	$status='';
	$con=getConnect();
	$query = "select mm.id as 'id',aa.anlagentyp as 'anlagentyp',aa.strasse as 'strasse',ee.title as 'equipment',mm.title as 'mangel',mm.description,mm.photo,uu.name as 'user',mm.status,mm.reportdate from Mangel as mm left join Equipments as ee on mm.equipment_id=ee.id left join Anlage as aa on ee.anlage_id=aa.id left join Users as uu on mm.user_id=uu.id where mm.title like '%{$mangel}%' and uu.name like '%{$user}%' and mm.status like '%{$status}%' and ee.anlage_id={$anlage};";
	$result = mysqli_query($con, $query);
	$temp = '<tr><th>Mangel</th><th>Description</th><th>Equipment</th><th>Status</th><th>Meldedatum</th><th>Priorit&auml;t Erh&ouml;hen</th></tr>';
	while ($row = mysqli_fetch_array($result)) {
		$temp .= "<tr>";
		$temp .= "<td>".$row["mangel"]."</td>";
		$temp .= "<td>".$row["description"]."</td>";
		$temp .= "<td>".$row["equipment"]."</td>";
		//$temp .= "<td>".$row["anlagentyp"]."</td>";
		//$temp .= "<td>".$row["strasse"]."</td>";
		//$temp .= "<td><a href='../".$row["photo"]."'>".$row["photo"]."</a></td>";
		//$temp .= "<td>".$row["user"]."</td>";
		$temp .= "<td>".$row["status"]."</td>";
		//$temp .= "<td>".date("d-m-Y H:i",$row["reportdate"])."</td>";
		$temp .= "<td>".$row["reportdate"]."</td>";
		$temp .= '<td><form action="PriorityRaised.php" method="get">';
		$temp .= '<input name="ID" hidden="true" type="text" value="'.$row["id"].'" size="10" maxlength="50">';
		$temp .= '<input class="btn" id="button" name="priority" type="submit" value="+"></form></td>';
		$temp .= "</tr>";
	}

	echo $temp;
	mysqli_close($con);
}


//<!-- TODO2 -->
//decomment this assignment after user sign in implementation
$user_id = $_SESSION['userid'];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Mangelreport Erstellen</title>
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="js/xhconn.js" type="text/javascript"></script>
<script src="js/mangelreport.js" type="text/javascript"></script>
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body id="mangelreport">
	<div id="wrapper">
	<?php include 'menu.php'; ?>
		<div id="textbereich">
			<h1>Mangel Einsenden</h1>
			<?php
			if(empty($_GET["send"])){
				$anlage = getSingleAnlage($_GET["ID"]);

				?>
			<h3>Gew&auml;hlte Anlage</h3>
			<table>
				<tr>
					<td>Anlagentyp:</td>
					<td><?php echo $anlage["Anlagentyp"]?></td>
				</tr>
				<tr>
					<td>Stra&szlig;e:</td>
					<td><?php echo $anlage["Strasse"]?></td>
				</tr>
				<tr>
					<td>Ort:</td>
					<td><?php echo $anlage["Ort"]?></td>
				</tr>
				<tr>
					<td>PLZ:</td>
					<td><?php echo $anlage["PLZ"]?></td>
				</tr>
			</table>
			<!--MAX UPDATE-->
			<h3>Bereits eingesendete M&auml;ngel</h3>
			<p>
				Kontrollieren Sie ob der von Ihnen gefundene Mangel bereits in
				nachfolgender Liste enthalten ist. Wenn JA besteht kein Bedarf einen
				weiteren Mangelbericht abzusenden. Erh&ouml;hen Sie stattdessen die
				<b>Bearbeitungs-Priorit&auml;t.</b>
			</p>
			<table class="mangelansicht">
			<?php
			getMangel($_GET["ID"]);
			?>
			</table>
			<!--MAX UPDATE End-->

			<h3>Mangelformular Ausf&uuml;llen</h3>
			<!-- TODO1 -->
			<form action="?send=1" method="post" enctype="multipart/form-data">
				<table>
					<tr>
						<td>Equipment:</td>
						<td><select id="lst_equipments" name="lst_equipments"
							onchange="refill_lst_mangels(this.value);">
								<option>[Bitte Ausw&auml;hlen]</option>
								<?php
								echo get_anlage_equipments_list($anlage["ID"]);;
								?>
						</select> <img id="loading_lst_equipments" class="hidden"
							src="images/loading.gif">
						</td>
					</tr>
					<tr>
						<td>Mangel:</td>
						<td><select id="lst_mangels" name="lst_mangels"
							onchange="mangel_changed(this.value);">
								<option>-</option>
						</select>
						</td>
					</tr>
					<tr id="new_mangel_tr" class="hidden">
						<td><b>Neue Mangel:</b></td>
						<td><input type="text" value="Neue Mangel" id="new_mangel"
							name="new_mangel" />
						</td>
					</tr>
					<tr>
						<td>Fehler Beschreibung:</td>
						<td><textarea id="description" name="description" rows="5"
								cols="30"></textarea>
						</td>
					</tr>
					<tr>
						<td>Bild Upload:</td>
						<td><input type="file" id="file_photo" name="file_photo"
							accept="image/*" /></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="checkbox" id="chk_feedback" name="chk_feedback" /><label
							for="chk_feedback">R&uuml;ckmeldung erhalten</label>
						</td>
					</tr>
					<tr>
						<td></td>
						<td><input class="btn" type="button" id="btn_Cancel"
							name="btn_Cancel" value="Abbrechen"
							onclick="window.history.back();" /> <input type="submit"
							class="btn" id="btn_Submit" name="btn_Submit" value="Senden" />
						</td>
					</tr>
				</table>
			</form>
			<?php
			}else{
				$is_error = false;
				$mangel_title="";
				if(!$user_id){
					$is_error = true;
					echo "<h4>Please Sign in!</h4>";
				}
				if(!$user_id){
					$is_error = true;
					echo "<h4>Please Sign in!</h4>";
				}

				if(isset($_POST["new_mangel"]) && $_POST["lst_mangels"]==-1){
					$new_mangel = trim($_POST["new_mangel"]);
					if(strlen($new_mangel)<=0){
						$is_error = true;
						echo "<h4>Please choose a valid \"Mangel\"!</h4>";
					}else{
						$mangel_title = $_POST["new_mangel"];
					}
				}else if(isset($_POST["lst_mangels"]) && strlen($_POST["lst_mangels"])>0){
					$mangel_title = $_POST["lst_mangels"];
				}else{
					$is_error = true;
					echo "<h4>Please choose a valid \"Mangel\"!</h4>";
				}
				if(isset($_POST["description"]) && strlen(trim($_POST["description"]))<=0){
					$is_error = true;
					echo "<h4>Please enter a description!</h4>";
				}
				if($is_error){
					echo "<p><input type=\"button\" value=\"Zur&uuml;ck\" onclick=\"window.history.back();\" /></p>";
				}else{
					$photo = "";
					if(isset($_FILES["file_photo"]) && $_FILES["file_photo"]["tmp_name"] ){
						$photo = save_photo();
					}
					$new_mangel_id = db_insert_mangel_report($_POST["lst_equipments"],$mangel_title, trim($_POST["description"]), $photo, $user_id,'new');
					if(isset($_POST["chk_feedback"]) && $_POST["chk_feedback"] && $new_mangel_id>0){
						db_insert_FeedbackSubscription($user_id,$new_mangel_id);
					}

					?>
			Your Report Registered. <br>
			<h3>Thanks for your Report.</h3>

			<?php
				}}
				?>

		</div>
	</div>




</body>
</html>
