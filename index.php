<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Startseite Wien-Instand</title>
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<meta http-equiv="content-language" content="de">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body id="startseite">
	<div id="wrapper">


	<?php include 'menu.php'; ?>


		<div id="textbereich">
			<p style="text-align: center;">
				<img src="images/home.png" style="margin: 0 auto;" width="680"
					height="358" />
			</p>
			<p>
				Diese Seite bietet einen Service zum einsenden von M&auml;ngeln f&uuml;r
				&ouml;ffentliche Sport- und Freizeitanlagen der Stadt Wien. Um einen
				neuen Report auszuf&uuml;llen m&uuml;ssen Sie zun&auml;chst die gew&uuml;nschte <a
					href="anlagensuche.php">Anlage ausw&auml;hlen</a>.
			</p>
		</div>
	</div>
</body>
</html>
